FROM debian:stable-slim
MAINTAINER Jean-Benoist Leger <jbleger@hds.utc.fr>

RUN \
    mkdir -p /usr/share/man/man1 /usr/share/man/man2 /usr/share/man/man3 /usr/share/man/man4 /usr/share/man/man5 /usr/share/man/man6 /usr/share/man/man7 /usr/share/man/man8 && \
    apt-get update && \
    apt-get install -y --no-install-recommends unzip python3-pip texlive-latex-recommended texlive-luatex texlive-xetex texlive-fonts-recommended cm-super-minimal texlive-fonts-extra dvipng pandoc wget && \
    pip3 install --break-system-packages jupyter jupyter-cache flatlatex matplotlib && \
    apt-get --purge -y remove texlive.\*-doc$ && \
    apt-get clean && \
    rm -rf /tmp/* /var/tmp/* && \
    wget https://github.com/quarto-dev/quarto-cli/releases/download/v1.3.433/quarto-1.3.433-linux-amd64.deb && \
    dpkg -i quarto-1.3.433-linux-amd64.deb && \
    rm quarto-1.3.433-linux-amd64.deb && \
    pip3 install --break-system-packages jax jaxlib torch numpy pytest sphinx sphinx_rtd_theme && \
    pip3 cache purge && \
    rm -rf /tmp/* /var/tmp/* && \
    mktexlsr && \
    updmap-sys && \
    luaotfload-tool --update --force && \
    true
